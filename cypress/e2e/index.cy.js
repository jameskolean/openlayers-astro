it("titles are correct", () => {
  const page = cy.visit("http://localhost:3000")

  page.title().should("contain", "Welcome to Astro.")
  page.get("h1").should("have.text", "Welcome to Astro")
})
