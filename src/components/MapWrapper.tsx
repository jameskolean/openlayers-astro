import Map from "ol/Map";
import OSM from "ol/source/OSM";
import styles from "./Map.module.css";
import TileLayer from "ol/layer/Tile";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import View from "ol/View";
import { fromLonLat } from "ol/proj";
import { useEffect, useRef, useState } from "preact/hooks";

interface Props {
  class?: string;
}
function MapWrapper(props: Props) {
  const [map, setMap] = useState<Map>();
  const [featuresLayer, setFeaturesLayer] =
    useState<VectorLayer<VectorSource>>();
  useEffect(() => {
    const initalFeaturesLayer = new VectorLayer({
      source: new VectorSource(),
    });

    const initialMap = new Map({
      target: mapElement.current ?? undefined,
      layers: [
        new TileLayer({
          source: new OSM(),
        }),
        initalFeaturesLayer,
      ],
      view: new View({
        projection: "EPSG:3857",
        center: fromLonLat([-88.913, 48.0]),
        zoom: 10,
      }),
      controls: [],
    });
    setMap(initialMap);
    setFeaturesLayer(initalFeaturesLayer);
  }, []);
  const mapElement = useRef<HTMLDivElement>(null);

  return (
    <div
      ref={mapElement}
      className={`${props.class} ${styles.mapWrapper}`}
    ></div>
  );
}

export default MapWrapper;
