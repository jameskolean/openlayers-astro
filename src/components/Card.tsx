import styles from "./Card.module.css";
interface Props {
  eta: number;
}
const Card = ({ eta }: Props) => {
  return (
    <div role="listitem" className={styles.wrapper}>
      <div className={styles.etaCircle}>
        <div className={styles.etaText}>
          {eta}
          <br />
          min
        </div>
      </div>
      <div className={styles.contents}>
        <div className={styles.linePlaceHolder}></div>
        <div className={styles.linePlaceHolder}></div>
        <div className={styles.linePlaceHolder}></div>
      </div>
    </div>
  );
};

export default Card;
