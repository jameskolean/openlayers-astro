import someJson from "./sample"
import { assert, expect, test } from "vitest"

test("JSON", () => {
  const output = JSON.stringify(someJson)

  expect(output).eq('{"foo":"hello","bar":"world"}')
  assert.deepEqual(JSON.parse(output), someJson, "matches original")
})
