import preact from "@astrojs/preact";
import webmanifest from "astro-webmanifest";
import { defineConfig } from "astro/config";
import robotsTxt from "astro-robots-txt";
import sitemap from "astro-sitemap";

// https://astro.build/config
export default defineConfig({
  site: "https://jameskolean.gitlab.io",
  // Mode not working anymore
  // base: MODE === 'development' ? '/' : '/openlayers-astro',
  base: process.env.PROD ? "/openlayers-astro" : "/",
  // GitLab Pages requires exposed files to be located in a folder called "public".
  // So we're instructing Astro to put the static build output in a folder of that name.
  outDir: "public",
  // The folder name Astro uses for static files (`public`) is already reserved
  // for the build output. So in deviation from the defaults we're using a folder
  // called `static` instead.
  publicDir: "static",
  integrations: [
    preact(),
    webmanifest({
      /**
       * required
       **/
      name: "openlayers-astro",

      /**
       * optional
       **/
      icon: "src/images/favicon.svg", // source for favicon & icons

      short_name: "openlayers-astro",
      description: "Here is your app description",
      start_url: "/",
      theme_color: "#000000",
      background_color: "#000000",
      display: "standalone",
    }),
    sitemap(),
    robotsTxt(),
  ],
});
